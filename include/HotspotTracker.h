#pragma once

#include <set>
#include <unordered_map>
#include <string>
#include <vector>
#include <cstdint>
#include <chrono>
#include <memory>

typedef std::chrono::time_point<std::chrono::steady_clock> Timestamp;

/// Tracks the N keys that occured most often in fixed periods of incoming keys
class HotspotTracker final
{
public:
  // --------------------------- cfg --------------------------- //

  /// size of the ring buffer that collects a chunk of the key stream
  static const size_t BUFF_LEN = 60'000;

  /// minimum occurence count of a key; keys with occurence counts below this
  /// value are not considered hotspots
  static const uint16_t MIN_FREQ;

  /// Time granted to a tracked hotspot after which it is removed. Each time the
  /// hotspot is encountered in a chunk, the hotspot's lifetime is reset to
  /// now() + HOTSPOT_TTL_MS.
  static const uint32_t HOTSPOT_TTL_MS;

  // ----------------------------------------------------------- //

  /// holds detailed information about a key
  class KeyDetail
  {
  private:
    std::string key;
    uint64_t freq;
    Timestamp tod; // time of death

  public:
    KeyDetail(const std::string &key) : key(key), freq(0) {}
    KeyDetail(const KeyDetail &) = default;
    ~KeyDetail() = default;

    bool operator<(const KeyDetail &other) const { return this->freq < other.freq; }
    KeyDetail &operator=(const KeyDetail &) = default;

    KeyDetail &bump_tod();

    // accessors and mutators

    const std::string &get_key() const { return key; }
    void set_key(const std::string &key) { this->key = move(key); }
    void set_freq(const uint64_t freq) { this->freq = freq; }
    const uint64_t get_freq() const { return this->freq; }
    const Timestamp &get_tod() const { return this->tod; }
  };

private:
  std::string key;

  /// number of tracked hotspots
  const uint32_t n = 0;

  /// current position in the ring buffer
  size_t key_buff_pos = 0;

  /// ring buffer which collectes a chunk of the key stream
  std::vector<std::string> key_buffer;

  /// contains the keys of the current set of tracked hotspots
  std::set<std::string> hotspot_keys;

  typedef std::shared_ptr<KeyDetail> KeyDetailPtr;

  struct KeyDetailCompareFreq
  {
    using is_transparent = void;

    bool operator()(const KeyDetailPtr &one, const KeyDetail &other) const
    {
      return *one < other;
    }

    bool operator()(const KeyDetail &one, const KeyDetailPtr &other) const
    {
      return one < *other;
    }

    bool operator()(const KeyDetailPtr &one, const KeyDetailPtr &other) const
    {
      return *one < *other;
    }
  };

  /// set of hotspots, sorted by frequency of occurence
  std::set<KeyDetailPtr, KeyDetailCompareFreq> hotspots_freq;

  struct KeyDetailCompareTod
  {
    using is_transparent = void;

    bool operator()(const KeyDetailPtr &one, const KeyDetail &other) const
    {
      return one->get_tod() < other.get_tod();
    }

    bool operator()(const KeyDetail &one, const KeyDetailPtr &other) const
    {
      return one.get_tod() < other->get_tod();
    }

    bool operator()(const KeyDetailPtr &one, const KeyDetailPtr &other) const
    {
      return one->get_tod() < other->get_tod();
    }
  };

  /// set of hotspots, sorted by time of death
  std::set<KeyDetailPtr, KeyDetailCompareTod> hotspots_ttl;

  /// hashtable that keeps key details of the current period for fast lookup 
  std::unordered_map<std::string, KeyDetail> key_details;

  void update_hotspots(KeyDetail &key_detail);

  /// remove the hotspots that have not been updated for HOTSPOT_TTL_MS time
  void prune_hotspots();

  /// add a new key detail as hotspot into the two sets
  void insert_hotspot(KeyDetail &key_detail);
  
public:
  HotspotTracker(const uint32_t n) : n(n), key_details(BUFF_LEN), key_buffer(BUFF_LEN) {}
  ~HotspotTracker() = default;
  HotspotTracker(const HotspotTracker &) = default;

  HotspotTracker &operator=(const HotspotTracker &) = default;

  /// performs hotspot tracking on a key
  /// complexity: O(2 log N)
  /// Note: this operation is not thread-safe
  void track(const std::string &key);

  /// determines if the key is a hotspot
  /// complexity: O(1)
  bool is_hotspot(const std::string &key);
  
  /// returns the N hotspots currently tracked, sorted by number of occurrence
  /// complexity: O(1)
  const std::set<KeyDetailPtr, KeyDetailCompareFreq> &get_hotspots() const { return this->hotspots_freq; }

  // for debugging purposes

  const size_t get_buff_len() { return BUFF_LEN; }
  const uint16_t get_min_freq() { return MIN_FREQ; }
  const uint32_t get_hotspot_ttl_ms() { return HOTSPOT_TTL_MS; }
  const size_t get_key_buff_pos() { return this->key_buff_pos; }
  std::unordered_map<std::string, KeyDetail> &get_key_details() { return this->key_details; }
};
