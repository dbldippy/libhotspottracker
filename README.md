# libHotspotTracker

libHotspotTracker is a shared C++ library that dynamically tracks the N most frequently encountered keys in a stream of input keys. If a tracked key is not encountered in the stream for a specific time, it is dropped. 

The library offers the following operations:

- `void track(key)` perform tracking on key string, O(2 log N)
- `bool is_hotspot(key)` determines if the key is a hotspot, O(1)
- `Set<Hotspot> get_hotspots()` Returns the N hotspots currently tracked, sorted by number of occurrence, O(1)

The tracker continually ingests string keys into a ring buffer. Keys are then mapped to occurrence and TTL in a hashtable for fast lookup, and their occurrence is incremented. Keys with have a minimal occurence count are deemed hotspot and enter the sorted set of hotspots. There, they can live for a specified time. If a hotspot key is not encountered in the stream for some time, it is erased from the set, else it's TTL is reset.


## Configuration

- `BUFF_LEN`: size of the ring buffer that collects a chunk of the key stream

- `MIN_FREQ` minimal occurence count before a key is considered a hotspot 

- `HOTSPOT_TTL_MS` <br>Time granted to a tracked hotspot after which it is removed. Each time the hotspot is encountered in a chunk, the hotspot's lifetime is reset to `now() + HOTSPOT_TTL_MS`.

The space complexity of the running algorithm is O(2`BUFF_LEN` + 3N).


# Concurrency

The `track` operation is not thread-safe. However, to execute it in parallel, each thread could instanciate its own tracker. Keys from the stream could then be passed to these trackers in a round-robin fashion. The hotspot query would perform `is_hotspot()` on each tracker. Analog, retrieving the hotspots calls `get_hotspots()` on all trackers.


# Build and Run

The library uses CMake 3.14.

To compile the library and all tests, run:

```
cmake -DCMAKE_BUILD_TYPE:STRING=Release -Bbuild -G Ninja
cmake --build build --config Release --target all
```

Build output is contained in the `/build` folder.

To execute all tests, run:

```
ctest --test-dir build
```

You can also run the individual test binaries:

```
build/hotspotTrackerTest
build/hotspotTrackerTTLTest
build/hotspotTrackerPerformanceTest
```

Please note that the performance test does not output correct runtimes when using ctest. To get accurate runtimes, please run the performance test executable.
