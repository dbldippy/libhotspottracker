#include <iostream>
#include <cassert>

#include <HotspotTracker.h>

using namespace std;
using namespace chrono;

const uint16_t HotspotTracker::MIN_FREQ = 2;
const uint32_t HotspotTracker::HOTSPOT_TTL_MS = 2'000;

// ------------------------ KeyDetail  ----------------------- //

HotspotTracker::KeyDetail &HotspotTracker::KeyDetail::bump_tod()
{
  this->tod = steady_clock::now() + milliseconds(HOTSPOT_TTL_MS);
  return *this;
}

// ---------------------- HotspotTracker --------------------- //

void HotspotTracker::track(const string &key)
{
  auto val_pair = this->key_details.emplace(key, KeyDetail(key));

  if (val_pair.second) // insertion successful
  {
    // move a new key into the key buffer
    this->key_buffer.at(this->key_buff_pos) = move(key); // pos is always kept in range

    // update the key detail hashtable
    auto val_pair = this->key_details.emplace(key, KeyDetail(key));

    // if the buffer wrapped-around, remove oldest key detail from hashtable.
    // This keeps the hashtable at size BUFF_LEN
    if (this->key_details.size() > this->BUFF_LEN)
    {
      size_t oldest_pos = (this->key_buff_pos + 1) % this->BUFF_LEN;
      string &oldest_key = this->key_buffer.at(oldest_pos); // pos is always kept in range

      this->key_details.erase(oldest_key);
      assert(this->key_details.size() == this->BUFF_LEN);
    }

    ++this->key_buff_pos %= this->BUFF_LEN;
  }

  update_hotspots(val_pair.first->second);
}

bool HotspotTracker::is_hotspot(const string &key)
{
  return this->hotspot_keys.find(key) != this->hotspot_keys.end();
}

// ------------------------- private ------------------------- //

// update the set of n hotspots with new key detail
void HotspotTracker::update_hotspots(KeyDetail &key_detail)
{
  auto min_it = this->hotspots_freq.begin();

  if (min_it == this->hotspots_freq.end())
  {
    // just insert
    if (key_detail.get_freq() + 1 >= this->MIN_FREQ)
    {
      key_detail.set_freq(key_detail.get_freq() + 1); // update key_detail in hashtable
      key_detail.bump_tod();
      insert_hotspot(key_detail);
      prune_hotspots();
      return;
    }
  }
  else if (this->hotspots_freq.size() <= this->n)
  {
    if (key_detail.get_freq() + 1 >= this->MIN_FREQ)
    {
      // extract() supports transparent comparison in C++23. Until then, a temp
      // allocation is necessary.
      auto nh = this->hotspots_freq.extract(make_shared<KeyDetail>(key_detail));

      if (!nh.empty()) // exists
      {
        nh.value()->set_freq(key_detail.get_freq() + 1);
        nh.value()->bump_tod();
        this->hotspots_freq.insert(move(nh));
        key_detail.set_freq(key_detail.get_freq() + 1);
      }
      else
      {
        key_detail.set_freq(key_detail.get_freq() + 1);
        key_detail.bump_tod();
        insert_hotspot(key_detail);
      }

      // maintain size n
      if (this->hotspots_freq.size() > this->n)
      {
        // remove smallest hotspot
        auto min_it = this->hotspots_freq.begin();

        this->hotspots_freq.erase(min_it);
        auto h = *min_it;
        this->hotspots_ttl.erase(h);
        this->hotspot_keys.erase(h->get_key());

        assert(this->hotspots_freq.size() == this->n);
      }

      prune_hotspots();
      return;
    }
  }

  key_detail.set_freq(key_detail.get_freq() + 1);
  prune_hotspots();
}

void HotspotTracker::prune_hotspots()
{
  // remove min if it's tod elapsed
  auto min_it = this->hotspots_ttl.begin();
  if (min_it != this->hotspots_ttl.end() && (*min_it)->get_tod() < steady_clock::now())
  {
    this->hotspots_ttl.erase(min_it);
    auto h = *min_it;
    this->hotspots_freq.erase(h);
    this->hotspot_keys.erase(h->get_key());
  }
}

void HotspotTracker::insert_hotspot(KeyDetail &key_detail)
{
  auto sp = make_shared<KeyDetail>(key_detail);
  this->hotspots_freq.insert(sp);
  this->hotspots_ttl.insert(sp);
  this->hotspot_keys.insert(key_detail.get_key());
}
