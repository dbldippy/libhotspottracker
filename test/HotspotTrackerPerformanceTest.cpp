#include <gtest/gtest.h>

#include <HotspotTracker.h>
#include <HotspotTrackerPerformanceTestEnv.h>

using namespace std;

auto *const _env =
    testing::AddGlobalTestEnvironment(new HotspotTrackerPerformanceTestEnv);

static HotspotTrackerPerformanceTestEnv &get_env()
{
  return *reinterpret_cast<HotspotTrackerPerformanceTestEnv *>(_env);
}

void print(const HotspotTracker &hotspot_tracker)
{
  for (const auto &h : hotspot_tracker.get_hotspots())
  {
    cout << h->get_key() << " " << h->get_freq() << endl;
  }
}

// -------------------------- tests -------------------------- //

TEST(HotspotTrackerTest, perf_equal_1m)
{
  const uint32_t N = 10;
  const uint32_t BURST_LEN = 1'000'000;

  HotspotTracker ht(N);

  string key = get_env().generate_key(0);

  for (size_t i = 0; i < BURST_LEN; ++i)
  {
    ht.track(key);
  }

  print(ht);
}

TEST(HotspotTrackerTest, perf_bigram_1m)
{
  const uint32_t N = 10;
  const uint32_t BURST_LEN = 1'000'000;

  HotspotTracker ht(N);

  for (auto &key : *get_env().keys_distinct_bigram_1m)
  {
    ht.track(key);
  }

  print(ht);
}

TEST(HotspotTrackerTest, perf_trigram_1m)
{
  const uint32_t N = 10;
  const uint32_t BURST_LEN = 1'000'000;

  HotspotTracker ht(N);

  for (auto &key : *get_env().keys_distinct_trigram_1m)
  {
    ht.track(key);
  }

  print(ht);
}

TEST(HotspotTrackerTest, perf_quadgram_1m)
{
  const uint32_t N = 10;
  const uint32_t BURST_LEN = 1'000'000;

  HotspotTracker ht(N);

  for (auto &key : *get_env().keys_distinct_quadgram_1m)
  {
    ht.track(key);
  }

  print(ht);
}
