#include <gtest/gtest.h>

#include <HotspotTracker.h>
#include <HotspotTrackerTestEnv.h>

using namespace std;

TEST(HotspotTrackerPerformanceTest, track_keys_equal_1m)
{
  const uint32_t N = 10;
  const uint32_t BURST_LEN = 1'000'000;

  HotspotTracker ht(N);

  string key = get_env().generate_key(0);

  for (size_t i = 0; i < BURST_LEN; ++i)
  {
    ht.track(key);
  }
}

TEST(HotspotTrackerPerformanceTest, track_keys_trigram_1m)
{
  const uint32_t N = 10;
  const uint32_t BURST_LEN = 1'000'000;

  HotspotTracker ht(N);

  for (auto &key : *get_env().keys_distinct_trigram_1m)
  {
    ht.track(key);
  }

  for (auto &h : ht.get_hotspots())
  {
    cout << h.get_key() << " " << h.get_freq() << endl;
  }
}
