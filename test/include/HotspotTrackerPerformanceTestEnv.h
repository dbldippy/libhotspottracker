#pragma once

#include <HotspotTrackerTestEnv.h>

class HotspotTrackerPerformanceTestEnv final : public HotspotTrackerTestEnv
{
public:
  KeySetPtr keys_unique_1m;
  KeySetPtr keys_distinct_bigram_1m;
  KeySetPtr keys_distinct_trigram_1m;
  KeySetPtr keys_distinct_quadgram_1m;

  void SetUp() override;
};
