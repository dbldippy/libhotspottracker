#pragma once

#include <vector>
#include <string>
#include <chrono>
#include <iostream>
#include <cstdio>
#include <memory>

#include <gtest/gtest.h>

/// CMake global test environment which generates test key sets  
class HotspotTrackerTestEnv : public testing::Environment
{
public:
  static const uint16_t KEY_LEN = 32;

  enum GenerateMode
  {
    UNIQUE,
    NGRAM,
    UNDEFINED
  };

  typedef std::unique_ptr<std::vector<std::string>> KeySetPtr;

  KeySetPtr keys_unique_1k;
  KeySetPtr keys_unique_60k;
  KeySetPtr keys_unique_100k;

  void SetUp() override;

  static KeySetPtr generate_keys(const GenerateMode mode, const uint64_t count,
                                 const uint64_t offset = 0, const uint64_t length = 0);

  static std::chrono::time_point<std::chrono::system_clock> start_watch();
  static void stop_watch(std::chrono::time_point<std::chrono::system_clock> start);

  static const std::string generate_key(const int i) // inline this one
  {
    char cfmt[KEY_LEN + 1];
    prep_fmt_str(cfmt);

    char cstr[KEY_LEN + 1];
    std::sprintf(cstr, cfmt, i);

    return std::string(cstr);
  }

  static const std::string random_string(const uint64_t len = KEY_LEN);
  static char *prep_fmt_str(char *cfmt, const uint64_t len = KEY_LEN);
};
