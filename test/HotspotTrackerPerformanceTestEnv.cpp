#include <HotspotTrackerPerformanceTestEnv.h>

using namespace std;

void HotspotTrackerPerformanceTestEnv::SetUp()
{
  const uint32_t KEY_COUNT = 100'000;

  this->keys_unique_1m = generate_keys(GenerateMode::UNIQUE, KEY_COUNT);
  this->keys_distinct_bigram_1m = generate_keys(GenerateMode::NGRAM, KEY_COUNT, 0, 2);
  this->keys_distinct_trigram_1m = generate_keys(GenerateMode::NGRAM, KEY_COUNT, 0, 3);
  this->keys_distinct_quadgram_1m = generate_keys(GenerateMode::NGRAM, KEY_COUNT, 0, 4);
}
