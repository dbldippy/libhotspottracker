#include <thread>
#include <chrono>

#include <gtest/gtest.h>

#include <HotspotTracker.h>
#include <HotspotTrackerTestEnv.h>

using namespace std;

auto *const _env =
    testing::AddGlobalTestEnvironment(new HotspotTrackerTestEnv);

static HotspotTrackerTestEnv &get_env()
{
  return *reinterpret_cast<HotspotTrackerTestEnv *>(_env);
}

TEST(HotspotTrackerTest, ttl_keys_2_bursts_2)
{
  const uint32_t N = 10;

  HotspotTracker ht(N);

  // 2

  string key = get_env().generate_key(2);
  ht.track(key);
  ht.track(key);
  ht.track(key);

  // 1

  key = get_env().generate_key(1);
  ht.track(key);
  ht.track(key);

  // 0

  key = get_env().generate_key(0);
  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 2);
  auto max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), 3);
  ASSERT_EQ((*(++max_it))->get_freq(), 2);

  uint16_t t = ht.get_hotspot_ttl_ms();
  this_thread::sleep_for(chrono::milliseconds(t + static_cast<uint16_t>(t / 2)));

  key = get_env().generate_key(3);
  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 1);

  key = get_env().generate_key(4);
  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 0);
}
