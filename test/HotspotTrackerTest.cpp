#include <gtest/gtest.h>

#include <HotspotTracker.h>
#include <HotspotTrackerTestEnv.h>

using namespace std;

auto *const _env =
    testing::AddGlobalTestEnvironment(new HotspotTrackerTestEnv);

static HotspotTrackerTestEnv &get_env()
{
  return *reinterpret_cast<HotspotTrackerTestEnv *>(_env);
}

TEST(HotspotTrackerTest, track_n_0_keys_unique_60k)
{
  const uint32_t N = 0;
  HotspotTracker ht(N);

  size_t i = 0;
  for (auto &key : *get_env().keys_unique_60k)
  {
    ht.track(key);

    ASSERT_EQ(ht.get_key_buff_pos(), (i + 1) % ht.get_buff_len());
    ASSERT_EQ(ht.get_hotspots().size(), 0);
    ASSERT_EQ(ht.get_key_details().size(), i + 1);

    ++i;
  }
}

TEST(HotspotTrackerTest, track_n_60k_keys_unique_60k)
{
  const uint32_t N = 60'000;
  HotspotTracker ht(N);

  size_t i = 0;
  for (auto &key : *get_env().keys_unique_60k)
  {
    ht.track(key);

    ASSERT_EQ(ht.get_key_buff_pos(), (i + 1) % ht.get_buff_len());
    ASSERT_EQ(ht.get_hotspots().size(), 0);
    ASSERT_EQ(ht.get_key_details().size(), i + 1);

    ++i;
  }
}

TEST(HotspotTrackerTest, track_n_100k_keys_unique_60k)
{
  const uint32_t N = 100'000;
  HotspotTracker ht(N);

  size_t i = 0;
  for (auto &key : *get_env().keys_unique_60k)
  {
    ht.track(key);

    ASSERT_EQ(ht.get_key_buff_pos(), (i + 1) % ht.get_buff_len());
    ASSERT_EQ(ht.get_hotspots().size(), 0);
    ASSERT_EQ(ht.get_key_details().size(), i + 1);

    ++i;
  }
}

TEST(HotspotTrackerTest, track_n_100_keys_unique_100k)
{
  const uint32_t N = 100;
  HotspotTracker ht(N);

  size_t i = 0;
  for (auto &key : *get_env().keys_unique_100k)
  {
    ht.track(key);

    ASSERT_EQ(ht.get_key_buff_pos(), (i + 1) % ht.get_buff_len());
    ASSERT_EQ(ht.get_hotspots().size(), 0);
    ASSERT_EQ(ht.get_key_details().size(), i < ht.get_buff_len() ? i + 1 : ht.get_buff_len());

    ++i;
  }
}

TEST(HotspotTrackerTest, track_n_10_keys_unique_60k_plus_1)
{
  const uint32_t N = 10;
  HotspotTracker ht(N);

  // auto start = get_env().start_watch();

  size_t i = 0;
  for (auto &key : *get_env().keys_unique_60k)
  {
    ht.track(key);

    ASSERT_EQ(ht.get_key_buff_pos(), (i + 1) % ht.get_buff_len());
    ASSERT_EQ(ht.get_hotspots().size(), 0);
    ASSERT_EQ(ht.get_key_details().size(), i + 1);

    ++i;
  }

  // get_env().stop_watch(start);

  ASSERT_EQ(ht.get_key_buff_pos(), 0);
  ASSERT_EQ(ht.get_hotspots().size(), 0);
  ASSERT_EQ(ht.get_key_details().size(), ht.get_buff_len());

  string key = get_env().generate_key(60'001);
  ht.track(key);

  ASSERT_EQ(ht.get_key_buff_pos(), 1);
  ASSERT_EQ(ht.get_hotspots().size(), 0);
  ASSERT_EQ(ht.get_key_details().size(), ht.get_buff_len());
}

TEST(HotspotTrackerTest, track_keys_2_bursts_2)
{
  const uint32_t N = 10;
  const uint16_t BURST_LEN = 2;

  HotspotTracker ht(N);

  // 2

  string key = get_env().generate_key(2);
  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 0);

  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 1);
  auto max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), 2);

  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 1);
  max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), 3);
  
  // 1

  key = get_env().generate_key(1);
  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 1);
  max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), 3);

  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 2);
  max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), 3);
  ASSERT_EQ((*(++max_it))->get_freq(), 2);

  // 0

  key = get_env().generate_key(0);
  ht.track(key);

  ASSERT_EQ(ht.get_hotspots().size(), 2);
  max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), 3);
  ASSERT_EQ((*(++max_it))->get_freq(), 2);
}

TEST(HotspotTrackerTest, track_keys_burst_5)
{
  const uint32_t N = 10;
  const uint16_t BURST_LEN = 5;

  HotspotTracker ht(N);

  for (auto &key : *get_env().keys_unique_1k)
  {
    ht.track(key);
  }

  string key = get_env().generate_key(60'000);

  ASSERT_EQ(ht.get_hotspots().size(), 0);

  for (size_t i = 0; i < BURST_LEN; ++i)
  {
    ht.track(key);
  }

  ASSERT_EQ(ht.get_hotspots().size(), 1);

  auto max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), BURST_LEN);
}

TEST(HotspotTrackerTest, track_keys_same_burst_different_buffers)
{
  const uint32_t N = 10;
  const uint16_t BURST_LEN = 20'000;

  HotspotTracker ht(N);

  // track 20k
  size_t i = 0;
  for (; i < BURST_LEN; ++i)
  {
    ht.track(get_env().generate_key(i));
  }

  // burst of 20k
  string key = get_env().generate_key(BURST_LEN);
  for (; i < 2 * BURST_LEN; ++i)
  {
    ht.track(key);
  }

  // track another 20k to fill up 60k buffer
  for (; i < ht.get_buff_len(); ++i)
  {
    ht.track(get_env().generate_key(i));
  }

  ASSERT_EQ(ht.get_hotspots().size(), 1);

  auto max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), BURST_LEN);

  // track 60k
  for (; i < 2 * ht.get_buff_len(); ++i)
  {
    ht.track(get_env().generate_key(i));
  }

  ASSERT_EQ(ht.get_hotspots().size(), 1);

  max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), BURST_LEN);

  // track 60k
  for (; i < 3 * ht.get_buff_len(); ++i)
  {
    ht.track(get_env().generate_key(i));
  }

  ASSERT_EQ(ht.get_hotspots().size(), 1);

  max_it = ht.get_hotspots().rbegin();
  ASSERT_EQ((*max_it)->get_freq(), BURST_LEN);
}

TEST(HotspotTrackerTest, is_hotspot_n_10_keys_equal_20k)
{
  const uint32_t N = 10;
  const uint16_t BURST_LEN = 20'000;

  HotspotTracker ht(N);

  string key = get_env().generate_key(0);

  for (size_t i = 0; i < BURST_LEN; ++i)
  {
    ht.track(key);
  }

  ASSERT_FALSE(ht.is_hotspot("a"));
  ASSERT_TRUE(ht.is_hotspot(key));
}
