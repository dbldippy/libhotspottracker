#include <cstdio>
#include <random>
#include <cassert>

#include <HotspotTrackerTestEnv.h>

using namespace std;

void HotspotTrackerTestEnv::SetUp()
{
  this->keys_unique_1k = generate_keys(GenerateMode::UNIQUE, 1'000);
  this->keys_unique_60k = generate_keys(GenerateMode::UNIQUE, 60'000);
  this->keys_unique_100k = generate_keys(GenerateMode::UNIQUE, 100'000);
}

HotspotTrackerTestEnv::KeySetPtr HotspotTrackerTestEnv::
    generate_keys(const GenerateMode mode, const uint64_t count,
                  const uint64_t offset, const uint64_t length)
{
  char cstr[KEY_LEN + 1];
  cstr[0] = '\0';

  char cfmt[KEY_LEN + 1];
  prep_fmt_str(cfmt);

  auto keys = make_unique<vector<string>>();

  switch (mode)
  {
  case GenerateMode::UNIQUE:
  {
    for (size_t i = offset; i < count + offset; ++i)
    {
      sprintf(cstr, cfmt, i);
      keys->push_back(string(cstr));
    }
  }
  break;
  case GenerateMode::NGRAM:
  {
    for (size_t i = 0; i < count; ++i)
    {
      auto str = random_string(length);
      while (str.length() < KEY_LEN)
      {
        str += str;
      }
      keys->push_back(str.substr(0, KEY_LEN));
    }
  }
  break;
  default:
    break;
  }

  return keys;
}

std::chrono::time_point<std::chrono::system_clock> HotspotTrackerTestEnv::start_watch()
{
  return chrono::system_clock::now();
}

void HotspotTrackerTestEnv::stop_watch(
    std::chrono::time_point<std::chrono::system_clock> start)
{
  auto end = chrono::system_clock::now();
  chrono::duration<double> diff = end - start;
  printf("duration: %f s\n", diff.count());
}

const std::string HotspotTrackerTestEnv::random_string(const uint64_t len)
{
  string str("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");

  random_device rd;
  mt19937 generator(rd());
  shuffle(str.begin(), str.end(), generator);

  return str.substr(0, len);
}

// ------------------------- private ------------------------- //

char *HotspotTrackerTestEnv::prep_fmt_str(char *cfmt, const uint64_t len)
{
  char cstr[len + 1];
  cstr[0] = '\0';

  cfmt[0] = '\0';
  sprintf(cfmt, "%%0%dd", len);

  return cfmt;
}
